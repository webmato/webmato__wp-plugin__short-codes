<?php

/*
Plugin Name: Webmato Short Codes
Description: [copyright_text year_since='1999' year_till='2016'  class_name='copyright' html_id='info-copyright' html_tag='div']Webmato[/copyright_text] ;
Version: 1.0.0
Author: Tomas Hrda
Repository: https://github.com/webmato/Wordpress-plugin-webmato_short-codes
Author URI: http://www.webmato.net
License: MIT
*/

/**
 * @attribute year_since - copyright year since | default = ''
 * @attribute year_till - copyright year till | default = actual year number
 * @attribute class_name - html class | default = 'copyright'
 * @attribute html_id - html id | default = ''
 * @attribute html_tag - embedded into the passed hmtl tag name | default = 'div'
 */
add_shortcode( 'copyright_text', function ( $attributes, $content, $tag ) {
  $year_since   = null;
  $year_till    = date( 'Y' );
  $class_name   = 'copyright';
  $html_id      = 'info-copyright';
  $html_tag     = 'div';
  $atts         = extract( shortcode_atts( array(
    'year_since' => null,
    'year_till'  => date( 'Y' ),
    'class_name' => 'copyright',
    'html_id'    => 'info-copyright',
    'html_tag'   => 'div',
  ), $attributes ) );
  $tag_openning = "<$html_tag class='$class_name' id='$html_id'>";
  $tag_closning = "</$html_tag>";
  $output       = $tag_openning . 'Copyright @' . ( $year_since ? $year_since . '-' : '' ) . $year_till . ' ' . $content . $tag_closning;

  return $output;
} );

